#!/usr/bin/env python3


while True:
    ok = False
    while not ok:
        saisie = input('Entrez un nombre (END pour quitter) : ').strip()
        if saisie == 'END':
            break
        ok = saisie.isnumeric()
        if not ok:
            print("Ce n'est pas un nombre entier.")

    if not ok:
        break

    # ok saisie est bien une chaîne de chiffres
    n = int(saisie)

    somme = 0
    for c in saisie:
        somme += int(c) 

    # alternative en calculant les chiffres
    somme2 = 0
    nn  = n
    while nn > 0:
        somme2 += nn % 10
        nn = nn // 10

    # Interrompt le programme si faux avec l'exception AssertionError
    assert somme2 == somme 

    print('Somme des chiffres :', somme)

    if somme % 3 == 0:
        print("OUI ! La somme des chiffres", somme, "est divisible par 3")
    else:
        print("NON ! La somme des chiffres", somme, "n'est pas divisible par 3")

    if n % 3 == 0:
        print("OUI ! Le nombre",n , "est divisible par 3")
    else:
        print("NON ! Le nombre",n , "n'est pas divisible par 3")

print('Bye.')
