#!/usr/bin/env python3

total    = 0
aliments = []
while True:
    ok = False
    while not ok:
        saisie = input('Entrez un aliment et un prix (END pour quitter) : ').strip()
        if saisie == 'END':
            break

        saisie_list = saisie.split()
        if len(saisie_list) < 2:
            print("Indiquez aliment et prix séparés par espace.")
            continue

        aliment, prix, *_ = saisie_list

        try:
            prix = float(prix)
        except ValueError:
            print("Le prix n'est pas un nombre.")
        else:
            ok = True
    
    if not ok:
        break
    if _:
        print("Infos supplémentaires ignorées :", *_)

    total += prix
    aliments.append(aliment)

print('Vous avez demandé :', *aliments)
print('Le prix total est :', total)
