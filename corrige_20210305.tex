\documentclass[12pt,a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{geometry}
\usepackage{fourier}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage[french]{babel}
\usepackage[hyphens]{url}
\usepackage{color}
\usepackage{listings}
\usepackage{bbm}

\definecolor{deepblue}{rgb}{0,0,0.5}
\definecolor{deepred}{rgb}{0.6,0,0}
\definecolor{deepgreen}{rgb}{0,0.5,0}
\DeclareFixedFont{\ttb}{T1}{txtt}{bx}{n}{12} % for bold
\DeclareFixedFont{\ttm}{T1}{txtt}{m}{n}{12}  % for normal

\newcommand\pythonstyle{\lstset{
language=Python,
%basicstyle=\ttm,
otherkeywords={self},             % Add keywords here
keywordstyle=\ttb\color{deepblue},
emph={MyClass,__init__},          % Custom highlighting
emphstyle=\ttb\color{deepred},    % Custom highlighting style
stringstyle=\color{deepgreen},
frame=tb,                         % Any extra options here
showstringspaces=false            % 
}}

\lstnewenvironment{python}[1][]
{
\pythonstyle
\lstset{#1}
}
{}

\newcommand\pythoninline[1]{{\pythonstyle\lstinline!#1!}}

\usepackage[%
    all,
    defaultlines=3 % nombre minimum de lignes
]{nowidow}


\parskip12pt
%\parindent0pt

\title{1APPD -- Conception d'applications}

\author{Jean-Pierre \textsc{Messager}\\[20pt] \textbf{Corrigé}} 

\date{\today} 
% \date{\today} date coulde be today 
% \date{25.12.00} or be a certain date
% \date{ } or there is no date 

\parskip5pt

\begin{document}
% Hint: \title{what ever}, \author{who care} and \date{when ever} could stand 
% before or after the \begin{document} command 
% BUT the \maketitle command MUST come AFTER the \begin{document} command! 
\maketitle

\emph{Livrables } : Ce document ainsi que le code source des programmes
en Python sont sur le dépôt Framagit \url{https://framagit.org/jpython/1appd}.

\section{Divisibilité par 3}

Nous nous proposons ici de vérifier puis de démontrer que si
un nombre est divisible par $3$ alors la somme de ses chiffres
l'est aussi, et réciproquement.

\subsection{Un programme Python pour vérifier sur quelques exemples}

Voir le dépôt GIT. Le calcul de la somme des chiffres est implémenté
de deux façons : en obtenant les chiffres à partir de la chaîne saisie
(itération sur les caractères qui la constituent) ou bien en récupérant
les restes successifs de divisions par dix.

L'instruction \emph{assert} vérifie que le résultat obtenu est bien
le même. En «~production~» il serait bien sûr inutile de faire deux
fois la même chose.

\subsection{Démontrons-le\,!}

\noindent\emph{a.} 
\[
    \begin{array}{cc}
    1842 &= (999 + 1) \times 1 + (99 + 1) \times 8 + (9 + 1) \times 4 + 2\\
    1842 &= ( 999 \times 1 + 99 \times 8 + 9 \times 4 ) + ( 1 + 8 + 4 + 2 )\\ 
    \end{array}
\]

Si un nombre s'écrit uniquement avec le chiffre $9$ en base $10$, il 
est divisible par 3, car :
\[
 \overbrace{99\ldots 9}^{\mbox{\footnotesize $k$ \emph{fois}}} =
 3 \times \overbrace{33\ldots 3}^{\mbox{\footnotesize $k$ \emph{fois}}}
\]

On remarque plus haut que $1842$ peut s'écrire comme la somme de termes
tous divisibles par $3$ et de la somme de ses chiffres en base $10$. S'il
est divisible par $3$ il peut s'écrire $3 \times N$ et la somme de ses
chiffres valant :
$ 3 \times N - (999 \times 1 + 99 \times 8 + \ldots) = 
3 \times N - 3*(333 \times 1 + 33 \times 8 + 3 \times 4) =
3 \times (N - 333 \times 1 - 33 \times 8 - 3 \times 4)
$ l'est aussi.

Réciproquement si la somme de ses chiffres est divisible par $3$, alors
$1842$ peut s'écrire comme la somme de deux nombres divisibles par $3$ et
donc il l'est aussi.

\noindent\emph{b.} Pour un nombre quelconque :
\[
    \begin{array}{ccc}
        N &=& a_{n}a_{n-1}...a_{1}{a_{0}}_{(10)} \\
        N &=& a_{n}\times 10^n + a_{n-1}\times 10^{n-1} + a_{1}\times 10 + a_{0}\\
        N &=& a_{n}\times (\underbrace{99...9}_{\mbox{\footnotesize $n$ \emph{fois}}} + 1)
        + a_{n-1}\times(\underbrace{99...9}_{\mbox{\footnotesize $n-1$ \emph{fois}}}+1) 
        + \ldots +  a_{1}\times (9 + 1) + a_{0}\\
        N &=& (a_{n}\times \underbrace{99...9}_{\mbox{\footnotesize $n$ \emph{fois}}} 
        + a_{n-1}\times\underbrace{99...9}_{\mbox{\footnotesize $n-1$ \emph{fois}}} 
        + \ldots +  a_{1}\times 9) + (a_n + a_{n-1} +\ldots + a_{0})\\
        N &=& 3\times(a_{n}\times \underbrace{33...3}_{\mbox{\footnotesize $n$ \emph{fois}}} 
        + a_{n-1}\times \underbrace{33...3}_{\mbox{\footnotesize $n-1$ \emph{fois}}} 
        + \ldots +  a_{1}\times 3) + (a_n + a_{n-1} +\ldots + a_{0})\\

    \end{array}
\]

Tout nombre $N$ peut donc s'écrire comme la somme d'un multiple de $3$ et
de la somme de ses chiffres en base 10. On en conclut donc qu'il est divisible
par $3$ si la somme de ses chiffres l'est et réciproquement.

\noindent\emph{c.} Les mêmes arguments s'appliquent quant à la divisibilité
par 9, en remarquant que :
\[
 \overbrace{99\ldots 9}^{\mbox{\footnotesize $k$ \emph{fois}}} =
 9 \times \overbrace{11\ldots 1}^{\mbox{\footnotesize $k$ \emph{fois}}} 
\
\]

et donc :
\[
        N = 9\times(a_{n}\times \underbrace{11...1}_{\mbox{\footnotesize $n$ \emph{fois}}} 
        + a_{n-1}\times \underbrace{11...1}_{\mbox{\footnotesize $n-1$ \emph{fois}}} 
        + \ldots +  a_{1}) + (a_n + a_{n-1} +\ldots + a_{0})
\]

Un nombre est donc divisible par $9$ si la somme de ses chiffres l'est, et
réciproquement.

\newpage
\subsection{\emph{Breakfast time!}}

Voir le dépôt GIT. Le seul ajout par rapport à l'énoncé est l'acceptation
de données en trop, auquel cas on avertit l'utilisateur :

\begin{flushleft}\texttt{%
Entrez un aliment et un prix (END pour quitter) : \textit{egg 12}\\
Entrez un aliment et un prix (END pour quitter) : \textit{egg 12 abcd}\\
Infos supplémentaires ignorées : abcd\\
Entrez un aliment et un prix (END pour quitter) : \textit{spam 10}\\
Entrez un aliment et un prix (END pour quitter) :\\
Indiquez aliment et prix séparés par espace.\\
Entrez un aliment et un prix (END pour quitter) : \textit{beans}\\
Indiquez aliment et prix séparés par espace.\\
Entrez un aliment et un prix (END pour quitter) : \textit{END}\\
Vous avez demandé : egg egg spam\\
Le prix total est : 34.0%
}
\end{flushleft}

\end{document}

